//
//  ViewController.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ViewController.h"
#import "Vertex.h"
#import "BaseEffect.h"
#import "ExampleModel.h"
#import "ExampleCubeModel.h"
#import "TextureManager.h"
#import "Settings.h"

@interface ViewController ()
    @end

@implementation ViewController {
    BaseEffect *_shader;
    ExampleModel *_model;
    TextureManager *_textureManager;
    GLKMatrix4 _viewMatrix;
}

    //MARK: UPDATE OPENGL FUNCTION
    - (void)update {
        //MARK: This is what makes the model spin!
        if (_model != nil) {
            //            [_model updateWithDelta:self.timeSinceLastUpdate];
        }
    }

    - (void)viewDidLoad {
        [super viewDidLoad];
        [self setupGLcontext];
        
        vertexCount = 0;
        
        if (vert == nil) {
            NSLog(@"Failed to process obj file.\r\n");
        } else {
            NSLog(@"Success!\r\n");
        }
        
        [self setupScene];
        [self setupTextures];
        [self setupModels];
    }

    - (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
        //Transfomr4: Viewport: Normalized -> Window
        //glViewport(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        
        //Sky Colour
        glClearColor(bg_red, bg_green, bg_blue, 1.0);  // Sky Colour
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        _viewMatrix = GLKMatrix4MakeTranslation(0, 0, -5);
        
        //MARK: Set our Fog Colour
        fogColor = GLKVector4Make(0.26, 0.70, 0.995, 1.0);  // blue
        //fogColor = GLKVector4Make(0.995, 0.60, 0.26, 1.0);  // orange
        //fogColor = GLKVector4Make(0.26, 0.996, 0.5, 1.0); // green
        _shader.fogColour = fogColor;
        
        //MARK: Render Updated Models Here!
        if (_model != nil) {
            [_model renderWithParentModelViewMatrix:_viewMatrix];
        }
    }

    - (void)setupGLcontext {
        GLKView *view = (GLKView *)self.view;
        view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        view.drawableDepthFormat = GLKViewDrawableDepthFormat16;
        [EAGLContext setCurrentContext:view.context];
    }

    - (void)setupScene {
        //MARK: Creates the glsl shader files
        _shader = [[BaseEffect alloc] initWithVertexShader:@"SimpleVertex.glsl" fragmentShader:@"SimpleFragment.glsl"];
        //MARK: Camera Persepective
        _shader.projectionMatrix = GLKMatrix4MakePerspective(
            GLKMathDegreesToRadians(40.0),
            self.view.bounds.size.width / self.view.bounds.size.height,
            0.01,
            150);
//        _shader.projectionMatrix = GLKMatrix4MakePerspective(
//            GLKMathDegreesToRadians(90.0),
//            (self.view.bounds.size.width / self.view.bounds.size.height),
//            1,
//            150);
    }

    - (void)setupTextures {
        _textureManager = [[TextureManager alloc] initWithTextures];
    }

    - (void)setupModels {
        _model = [[ExampleModel alloc] initWithShader:_shader];
        GLuint texture = [_textureManager getTexture:0];
        [_model setTexture: texture];
        [_model setColor:0.0 :0.0 :1.0];
        [_model setPosition:0.0 :0.0 :0.0];
        [_model setScale: 0.5];
    }
@end



