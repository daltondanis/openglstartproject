//
//  TextureManager.h
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;

@interface TextureManager : NSObject
    - (id)initWithTextures;
    - (GLuint)textureLoader:(NSString *)file;
    - (void)loadTextures;
    - (GLuint)getTexture:(int)value;
@end


