//
//  ExampleModel.h
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"

@interface ExampleModel : Model
    - (instancetype)initWithShader:(BaseEffect *)shader;
    - (instancetype)initWithTexture:(BaseEffect *)shader :(GLuint)texture;
    - (void)updateWithDelta:(NSTimeInterval)dt;
@end

