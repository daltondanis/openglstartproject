//
//  Model.h
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vertex.h"

@class BaseEffect;
@import GLKit;

@interface Model : NSObject
    @property (nonatomic, strong) BaseEffect *shader;
    @property (nonatomic) GLuint texture;
    
    @property (nonatomic, assign) GLKVector3 position;
    @property (nonatomic) float rotationX;
    @property (nonatomic) float rotationY;
    @property (nonatomic) float rotationZ;
    @property (nonatomic) float scaleX;
    @property (nonatomic) float scaleY;
    @property (nonatomic) float scaleZ;
    
    @property (nonatomic) float red;
    @property (nonatomic) float green;
    @property (nonatomic) float blue;
    
    //Init Functions
    - (instancetype)initWithName:(char *)name shader:(BaseEffect *)shader vertices:(Vertex *)vertices vertexCount:(unsigned int)vertexCount;
    
    //Main Functions
    - (void)renderWithParentModelViewMatrix:(GLKMatrix4)parentModelViewMatrix;
    - (void)updateWithDelta:(NSTimeInterval)dt;
    
    //Other Functions
    //- (void)loadTexture:(NSString *)filename;
    - (void)getModelInfo;
    - (void)setColor:(float)red :(float)green :(float)blue;
    - (void)setTextureFile:(GLuint)file;
    - (void)setPosition:(float)x :(float)y :(float)z;
    - (void)setScale:(float)scale;
    - (void)setRotation:(float)x :(float)y :(float)z;
@end


