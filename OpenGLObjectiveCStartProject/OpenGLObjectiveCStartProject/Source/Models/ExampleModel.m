//
//  ExampleModel.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ExampleModel.h"

@implementation ExampleModel
    
    const static Vertex vertices[] = {
        //NOTE THIS IS USING TRIANGLES INSTEAD OF SQUARES
        
        //MARK: This is a square setup
//        // x   y  z    r  g  b  a    u  v    nx ny nz
//        {{ 1, -1, 0}, {1, 0, 0, 1}, {1, 0}, {0, 0, 1}}, // V0
//        {{ 1,  1, 0}, {0, 1, 0, 1}, {1, 1}, {0, 0, 1}}, // V1
//        {{-1,  1, 0}, {0, 0, 1, 1}, {0, 1}, {0, 0, 1}}, // V2
//        {{-1, -1, 0}, {0, 0, 0, 0}, {0, 0}, {0, 0, 1}} // V3
        
        //MARK: This is a triangle setup
        // x   y  z    r  g  b  a    u  v    nx ny nz
        {{ 1, -1, 0}, {1, 0, 0, 1}, {1, 0}, {0, 0, 1}}, // V0
        {{ 1,  1, 0}, {0, 1, 0, 1}, {1, 1}, {0, 0, 1}}, // V1
        {{-1,  1, 0}, {0, 0, 1, 1}, {0, 1}, {0, 0, 1}}, // V2
        
        {{-1,  1, 0}, {0, 0, 1, 1}, {0, 1}, {0, 0, 1}}, // V3
        {{-1, -1, 0}, {0, 0, 0, 1}, {0, 0}, {0, 0, 1}}, // V4
        {{ 1, -1, 0}, {1, 0, 0, 1}, {1, 0}, {0, 0, 1}}, // V5
    };
    
    // for indexed buffering only.  We switched to ordered.
    const static GLubyte indexList[] = {
        0, 1, 2,
        2, 3, 0
    };
    
    - (instancetype)initWithShader:(BaseEffect *)shader {
        if ((self = [super initWithName:"square" shader:shader vertices:(Vertex *)vertices vertexCount:sizeof(vertices)/sizeof(vertices[0])])) {
            
        }
        return self;
    }
    
    - (instancetype)initWithTexture:(BaseEffect *)shader :(GLuint)texture {
        if ((self = [super initWithName:"square" shader:shader vertices:(Vertex *)vertices vertexCount:sizeof(vertices)/sizeof(vertices[0])])) {
            
        }
        return self;
    }
    
    - (void)updateWithDelta:(NSTimeInterval)dt {
//        float secsPerMove = 2;
//        self.position = GLKVector3Make(sinf(CACurrentMediaTime() * 2*M_PI / secsPerMove), self.position.y, self.position.z);
    }
    
@end


