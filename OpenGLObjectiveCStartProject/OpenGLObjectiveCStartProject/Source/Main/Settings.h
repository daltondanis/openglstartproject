//
//  Settings.h
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-09.
//  Copyright © 2018 opengl. All rights reserved.
//

//#ifndef Settings_h
//#define Settings_h
//
//
//#endif /* Settings_h */

#import <Foundation/Foundation.h>
@import OpenGLES;
@import GLKit;

@interface Settings : NSObject

    extern const int exampeInt;
    extern const NSString* exampeString;

    //MARK: Background colors
    extern const float bg_red;
    extern const float bg_green;
    extern const float bg_blue;
@end


