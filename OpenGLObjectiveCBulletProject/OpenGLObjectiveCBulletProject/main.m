//
//  main.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
