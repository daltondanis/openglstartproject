//
//  TextureManager.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "TextureManager.h"

@implementation TextureManager {
    GLuint _textures[2];

}
    //MARK: to load a texture put the following in the array:
    //Loads in the texture files by <filename>.<filetype>
    const static NSString *filenames[2] = {
        @"brick.png",
        @"dice.png"
    };

    - (id)initWithTextures {
        NSLog(@"Loading all texture files");
        [self loadTextures];
        return self;
    }

    - (GLuint)textureLoader:(NSString *)file {
        NSError *error;
        NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:nil];
        NSDictionary *options = @{ GLKTextureLoaderOriginBottomLeft: @YES };
        GLKTextureInfo *info = [GLKTextureLoader textureWithContentsOfFile:path options:options error:&error];
        if (info == nil) {
            NSLog(@"Error loading file: %@", error.localizedDescription);
        } else {
            return info.name;
        }
        return 0;
    }

    - (void)loadTextures {
        for (int i = 0; i < 2; i++) {
            _textures[i] = [self textureLoader:(NSString *)filenames[i]];
        }
    }

    - (GLuint)getTexture:(int)value {
        NSLog(@"Return Texture: %i", value);
        return _textures[value];
    }

@end

