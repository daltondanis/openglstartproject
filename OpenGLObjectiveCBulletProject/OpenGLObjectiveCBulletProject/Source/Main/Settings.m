//
//  Settings.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-09.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Settings.h"

@implementation Settings
    //MARK: Examples
    const int exampeInt = 0;
    const NSString* exampeString = @"Settings Example";

    //MARK: Background colors
    const float bg_red = 0.0;
    const float bg_green = 1.0;
    const float bg_blue = 0.0;
    
@end
