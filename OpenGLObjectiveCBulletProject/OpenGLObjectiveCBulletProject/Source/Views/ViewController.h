//
//  ViewController.h
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vertex.h"

@import GLKit;

@interface ViewController : GLKViewController {
    GLKVector4 fogColor;
    Vertex *vert;
    long vertexCount;
}
    @end

