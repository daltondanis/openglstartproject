//
//  ViewController.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ViewController.h"
#import "Vertex.h"
#import "BaseEffect.h"
#import "Settings.h"
#import "GameScene.h"
#import "TextureManager.h"

@interface ViewController ()
    @end

@implementation ViewController {
    BaseEffect *_shader;
    TextureManager *_textureManager;
//    GLKMatrix4 _viewMatrix;
    GameScene *_gamescene;
}

    //MARK: UPDATE OPENGL FUNCTION
    - (void)update {
        //MARK: Update the scene
        if (_gamescene != nil) {
            [_gamescene updateWithDelta:self.timeSinceLastUpdate];
        }
    }

    - (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        [_gamescene touchesBegan:touches withEvent:event];
    }

    - (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
        [_gamescene touchesMoved:touches withEvent:event];
    }

    - (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
        [_gamescene touchesEnded:touches withEvent:event];
    }

    - (void)viewDidLoad {
        [super viewDidLoad];
        [self setupGLcontext];
        [self setupTextures];
        
        vertexCount = 0;
        
        //Not used
//        if (vert == nil) {
//            NSLog(@"Failed to process obj file.\r\n");
//        } else {
//            NSLog(@"Success!\r\n");
//        }
        [self setupScene];
        [self setupModels];
    }

    - (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
        //Transfomr4: Viewport: Normalized -> Window
        //glViewport(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        
        //Sky Colour
        glClearColor(bg_red, bg_green, bg_blue, 1.0);  // Sky Colour
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        //NOTE: Not used as the viewMatrix is the GameScene Model matrix
        //_viewMatrix = GLKMatrix4MakeTranslation(0, 0, -5);
        
        //MARK: Set our Fog Colour
        fogColor = GLKVector4Make(0.26, 0.70, 0.995, 1.0);  // blue
        //fogColor = GLKVector4Make(0.995, 0.60, 0.26, 1.0);  // orange
        //fogColor = GLKVector4Make(0.26, 0.996, 0.5, 1.0); // green
        _shader.fogColour = fogColor;
        
        //MARK: Render Updated Models Here!
//        if (_model != nil) {
//            [_model renderWithParentModelViewMatrix:_viewMatrix];
//        }
        
        //MARK: Render Scene Here!
        if (_gamescene != nil) {
            [_gamescene renderWithParentModelViewMatrix:GLKMatrix4Identity];
        }
    }

    - (void)setupGLcontext {
        GLKView *view = (GLKView *)self.view;
        view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        view.drawableDepthFormat = GLKViewDrawableDepthFormat16;
        [EAGLContext setCurrentContext:view.context];
        
//        glViewport(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }

    - (void)setupScene {
        //MARK: Creates the glsl shader files
        _shader = [[BaseEffect alloc] initWithVertexShader:@"SimpleVertex.glsl" fragmentShader:@"SimpleFragment.glsl"];
        //MARK: Camera Persepective
        GLKMatrix4 projection = GLKMatrix4MakePerspective(
                                  GLKMathDegreesToRadians(85.0),
                                  self.view.bounds.size.width / self.view.bounds.size.height,
                                  0.1,
                                  150);
        _shader.projectionMatrix = projection;
//        _shader.projectionMatrix = GLKMatrix4MakePerspective(
//            GLKMathDegreesToRadians(90.0),
//            (self.view.bounds.size.width / self.view.bounds.size.height),
//            1,
//            150);
        
        //MARK: Setup scene
        _gamescene = [[GameScene alloc] initWithShader:_shader];
        _gamescene.view = self.view;
        _gamescene.projectionMatrix = projection;
        _gamescene.textureManager = _textureManager;
        [_gamescene renderModels];
    }

    - (void)setupTextures {
        _textureManager = [[TextureManager alloc] initWithTextures];
    }

    - (void)setupModels {
        //Moved to GameScene!
    }

    - (void)setupPhysics {
        
    }
@end



