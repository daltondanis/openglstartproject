//
//  ExampleCircleModel.h
//  OpenGLObjectiveCBulletProject
//
//  Created by Dalton Danis on 2018-04-10.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "PModel.h"

@interface ExampleCircleModel : PModel
- (instancetype)initWithShader:(BaseEffect *)shader;
- (instancetype)initWithShader:(BaseEffect *)shader :(GLuint)texture;
- (void)updateWithDelta:(NSTimeInterval)dt;
- (void)setScale:(float)scale;
- (void)setScale:(float)x :(float)y :(float)z;
@end
