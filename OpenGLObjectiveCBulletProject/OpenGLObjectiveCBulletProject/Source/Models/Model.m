//
//  Model.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"
#import "BaseEffect.h"

@import Foundation;
@import GLKit;
@import OpenGLES;

@implementation Model {
    char *_name;
    GLuint _vao;
    GLuint _vertexBuffer;
    GLuint _indexBuffer;
    unsigned int _vertexCount;
    unsigned int _indexCount;
    BaseEffect *_shader;
}
    
- (instancetype)initWithName:(char *)name shader:(BaseEffect *)shader vertices:(Vertex *)vertices vertexCount:(unsigned int)vertexCount {
    
    if ((self = [super init])) {
        if (vertexCount != 0) {
            NSLog(@"Vertices Count: %f", vertices[0].Position[0]);
        }
        NSLog(@"Count: %i", vertexCount);
        _name = name;
        _vertexCount = vertexCount;
//        _indexCount = indexCount;
        _shader = shader;
        self.position = GLKVector3Make(0, 0, 0);
        self.rotationX = 0;
        self.rotationY = 0;
        self.rotationZ = 0;
        self.scaleX = 1.0;
        self.scaleY = 1.0;
        self.scaleZ = 1.0;
        self.red = 0.0;
        self.green = 0.0;
        self.blue = 1.0;
        self.children = [NSMutableArray array];
        
        if (vertices) {
            glGenVertexArraysOES(1, &_vao);
            glBindVertexArrayOES(_vao);
            
            // Generate vertex buffer
            glGenBuffers(1, &_vertexBuffer);
            glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vertex), vertices, GL_DYNAMIC_DRAW);
            
            // Generate index buffer
            /*glGenBuffers(1, &_indexBuffer);
             glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);
             glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLubyte), indices, GL_STATIC_DRAW);
             */
            
            // Enable vertex attributes
            glEnableVertexAttribArray(VertexAttribPosition);
            glVertexAttribPointer(
                                  VertexAttribPosition,
                                  3,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  sizeof(Vertex),
                                  (const GLvoid *) offsetof(Vertex, Position));
            
            glEnableVertexAttribArray(VertexAttribColor);
            glVertexAttribPointer(
                                  VertexAttribColor,
                                  4,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  sizeof(Vertex),
                                  (const GLvoid *) offsetof(Vertex, Color));
            
            glEnableVertexAttribArray(VertexAttribTexCoord);
            glVertexAttribPointer(
                                  VertexAttribTexCoord,
                                  2,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  sizeof(Vertex),
                                  (const GLvoid *) offsetof(Vertex, TexCoord));
            
            // diffuse
            glEnableVertexAttribArray(VertexAttribNormal);
            glVertexAttribPointer(
                                  VertexAttribNormal,
                                  3,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  sizeof(Vertex),
                                  (const GLvoid *) offsetof(Vertex, Normal));
            
            glBindVertexArrayOES(0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }
    }
    return self;
}

- (GLKMatrix4)modelMatrix {
    GLKMatrix4 modelMatrix = GLKMatrix4Identity;
    modelMatrix = GLKMatrix4Translate(modelMatrix, self.position.x, self.position.y, self.position.z);
    modelMatrix = GLKMatrix4Rotate(modelMatrix, self.rotationX, 1, 0, 0);
    modelMatrix = GLKMatrix4Rotate(modelMatrix, self.rotationY, 0, 1, 0);
    modelMatrix = GLKMatrix4Rotate(modelMatrix, self.rotationZ, 0, 0, 1);
    modelMatrix = GLKMatrix4Scale(modelMatrix, self.scaleX, self.scaleY, self.scaleZ);
    return modelMatrix;
}
    
- (void)renderWithParentModelViewMatrix:(GLKMatrix4)parentModelViewMatrix {
    //MARK: Renders children
//    NSLog(@"Rendering");
    GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply(parentModelViewMatrix, [self modelMatrix]);
    for (Model * child in self.children) {
//        NSLog(@"Rendering child");
        [child renderWithParentModelViewMatrix:modelViewMatrix];
    }
    
    //OPTIONAL: If doesn't haver anything don't render
//    if (_vao == 0) return;
    
//    GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply(parentModelViewMatrix, [self modelMatrix]);
    _shader.modelMatrix = [self modelMatrix];
    _shader.modelViewMatrix = modelViewMatrix;
    _shader.texture = self.texture;
    [_shader prepareToDraw];
    //MARK: Remeber to do shader work just after prepareToDraw() function call
    glUniform4f(_shader.colorUniform, (GLfloat) self.red, (GLfloat) self.green, (GLfloat) self.blue, 1);
    glBindVertexArrayOES(_vao);
    //glDrawElements(GL_TRIANGLES, _indexCount, GL_UNSIGNED_BYTE, 0);  // indexed drawing
    glDrawArrays(GL_TRIANGLES, 0, _vertexCount);  // ORDERED DRAWING
    glBindVertexArrayOES(0);
    
}
    
- (void)updateWithDelta:(NSTimeInterval)dt {
    //Does something during every update
    //MARK: Updates children objects
    for (Model* child in self.children) {
        [child updateWithDelta:dt];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)getModelInfo {
    NSLog(@"\n-------- Model --------");
    NSLog(@"POS: X \(self.position.x) Y \(self.position.y) ");
}
    
- (void)setColor:(float)red :(float)green :(float)blue {
    self.red = red;
    self.green = green;
    self.blue = blue;
}
    
- (void)setTextureFile:(GLuint)file {
    self.texture = file;
}
    
- (void)setPosition:(float)x :(float)y :(float)z {
    self.position = GLKVector3Make(x, y, z);
}
    
- (void)setScale:(float)scale {
    self.scaleX = scale;
    self.scaleY = scale;
    self.scaleZ = scale;
}

- (void)setScale:(float)x :(float)y :(float)z {
    self.scaleX = x;
    self.scaleY = y;
    self.scaleZ = z;
}

- (void)setRotation:(float)x :(float)y :(float)z {
    self.rotationX = x;
    self.rotationY = y;
    self.rotationZ = z;
}

- (void)setBoundingBox:(Vertex *)vertices {
    float minimum = vertices[0].Position[0];
    float maximum = vertices[0].Position[0];
    for (int i = 0; i < (sizeof(vertices) / sizeof(vertices[0])); i++) {
        if (vertices[i].Position[0] >= maximum) { //x
            maximum = vertices[i].Position[0];
        } else if (vertices[i].Position[0] <= minimum) {
            minimum = vertices[i].Position[0];
        }
    }
    float width = maximum - minimum;
    
    minimum = vertices[0].Position[0];
    maximum = vertices[0].Position[0];
    for (int i = 0; i < (sizeof(vertices) / sizeof(vertices[0])); i++) {
        if (vertices[i].Position[1] >= maximum) { //x
            maximum = vertices[i].Position[1];
        } else if (vertices[i].Position[1] <= minimum) {
            minimum = vertices[i].Position[1];
        }
    }
    float height = maximum - minimum;
    
    minimum = vertices[0].Position[0];
    maximum = vertices[0].Position[0];
    for (int i = 0; i < (sizeof(vertices) / sizeof(vertices[0])); i++) {
        if (vertices[i].Position[2] >= maximum) { //x
            maximum = vertices[i].Position[2];
        } else if (vertices[i].Position[2] <= minimum) {
            minimum = vertices[i].Position[2];
        }
    }
    float depth = maximum - minimum;
    
    self.width = width * self.scaleX;
    self.height = height * self.scaleY;
    self.depth = depth * self.scaleZ;
    // Another option to store the size
    //GLKVector3Make(width * self.scaleX, height * self.scaleY, depth * self.scaleZ);
}
    
@end


