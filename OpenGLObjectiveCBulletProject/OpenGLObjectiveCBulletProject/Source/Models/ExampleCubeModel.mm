//
//  ExampleCubeModel.m
//  OpenGLObjectiveCStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ExampleCubeModel.h"

@implementation ExampleCubeModel
    
    const static Vertex vertices[] = {
        // Front
        {{1, -1, 1}, {1, 0, 0, 1}, {0.25, 0}, {0, 0, 1}},  // 0
        {{1, 1, 1}, {0, 1, 0, 1}, {0.25, 0.25}, {0, 0, 1}},   // 1
        {{-1, 1, 1}, {0, 0, 1, 1}, {0, 0.25}, {0, 0, 1}},  // 2
        
        {{-1, 1, 1}, {0, 0, 1, 1}, {0, 0.25}, {0, 0, 1}},  // 2
        {{-1, -1, 1}, {0, 0, 0, 1}, {0, 0}, {0, 0, 1}}, // 3
        {{1, -1, 1}, {1, 0, 0, 1}, {0.25, 0}, {0, 0, 1}},  // 0
        
        // Back
        {{-1, -1, -1}, {0, 0, 1, 1}, {0.5, 0}, {0, 0, -1}}, // 4
        {{-1, 1, -1}, {0, 1, 0, 1}, {0.5, 0.25}, {0, 0, -1}},  // 5
        {{1, 1, -1}, {1, 0, 0, 1}, {0.25, 0.25}, {0, 0, -1}},   // 6
        
        {{1, 1, -1}, {1, 0, 0, 1}, {0.25, 0.25}, {0, 0, -1}},   // 6
        {{1, -1, -1}, {0, 0, 0, 1}, {0.25, 0}, {0, 0, -1}},  // 7
        {{-1, -1, -1}, {0, 0, 1, 1}, {0.5, 0}, {0, 0, -1}}, // 4
        
        // Left
        {{-1, -1, 1}, {1, 0, 0, 1}, {0.75, 0}, {-1, 0, 0}},  // 8
        {{-1, 1, 1}, {0, 1, 0, 1}, {0.75, 0.25}, {-1, 0, 0}},   // 9
        {{-1, 1, -1}, {0, 0, 1, 1}, {0.5, 0.25}, {-1, 0, 0}},  // 10
        
        {{-1, 1, -1}, {0, 0, 1, 1}, {0.5, 0.25}, {-1, 0, 0}},  // 10
        {{-1, -1, -1}, {0, 0, 0, 1}, {0.5, 0}, {-1, 0, 0}}, // 11
        {{-1, -1, 1}, {1, 0, 0, 1}, {0.75, 0}, {-1, 0, 0}},  // 8
        
        // Right
        {{1, -1, -1}, {1, 0, 0, 1}, {1, 0}, {1, 0, 0}}, // 12
        {{1, 1, -1}, {0, 1, 0, 1}, {1, 0.25}, {1, 0, 0}},  // 13
        {{1, 1, 1}, {0, 0, 1, 1}, {0.75, 0.25}, {1, 0, 0}},   // 14
        
        {{1, 1, 1}, {0, 0, 1, 1}, {0.75, 0.25}, {1, 0, 0}},   // 14
        {{1, -1, 1}, {0, 0, 0, 1}, {0.75, 0}, {1, 0, 0}},  // 15
        {{1, -1, -1}, {1, 0, 0, 1}, {1, 0}, {1, 0, 0}}, // 12
        
        // Top
        {{1, 1, 1}, {1, 0, 0, 1}, {0.25, 0.25}, {0, 1, 0}},   // 16
        {{1, 1, -1}, {0, 1, 0, 1}, {0.25, 0.5}, {0, 1, 0}},  // 17
        {{-1, 1, -1}, {0, 0, 1, 1}, {0, 0.5}, {0, 1, 0}}, // 18
        
        {{-1, 1, -1}, {0, 0, 1, 1}, {0, 0.5}, {0, 1, 0}}, // 18
        {{-1, 1, 1}, {0, 0, 0, 1}, {0, 0.25}, {0, 1, 0}},  // 19
        {{1, 1, 1}, {1, 0, 0, 1}, {0.25, 0.25}, {0, 1, 0}},   // 16
        
        // Bottom
        {{1, -1, -1}, {1, 0, 0, 1}, {0.5, 0.25}, {0, -1, 0}},  // 20
        {{1, -1, 1}, {0, 1, 0, 1}, {0.5, 0.5}, {0, -1, 0}},   // 21
        {{-1, -1, 1}, {0, 0, 1, 1}, {0.25, 0.5}, {0, -1, 0}},  // 22
        
        {{-1, -1, 1}, {0, 0, 1, 1}, {0.25, 0.5}, {0, -1, 0}},  // 22
        {{-1, -1, -1}, {0, 0, 0, 1}, {0.25, 0.25}, {0, -1, 0}}, // 23
        {{1, -1, -1}, {1, 0, 0, 1}, {0.5, 0.25}, {0, -1, 0}}  // 20
    };
    
    const static GLubyte indexList[] = {
        // Front
        0, 1, 2,
        2, 3, 0,
        
        // Back
        4, 5, 6,
        6, 7, 4,
        
        // Left
        8, 9, 10,
        10, 11, 8,
        
        // Right
        12, 13, 14,
        14, 15, 12,
        
        // Top
        16, 17, 18,
        18, 19, 16,
        
        // Bottom
        20, 21, 22,
        22, 23, 20
    };
    
    - (instancetype)initWithShader:(BaseEffect *)shader {
        if ((self = [super initWithName:"cube" mass: 1.0f convex: YES tag: kBallTag shader:shader vertices:(Vertex *)vertices vertexCount:sizeof(vertices)/sizeof(vertices[0])])) {
            [self setBoundingBox:(Vertex *)vertices];
        }
        return self;
    }
    
    - (instancetype)initWithShader:(BaseEffect *)shader :(GLuint)texture {
        if ((self = [super initWithName:"cube" mass: 1.0f convex: YES tag: kBallTag shader:shader vertices:(Vertex *)vertices vertexCount:sizeof(vertices)/sizeof(vertices[0])])) {
            [self setTextureFile:texture];
            [self setBoundingBox:(Vertex *)vertices];
        }
        return self;
    }
    
    - (void)updateWithDelta:(NSTimeInterval)dt {
//        self.rotationZ += M_PI * dt;
//        self.rotationY += M_PI/8 * dt;
    }

    - (void)setScale:(float)scale {
        [super setScale:scale];
        [self setBoundingBox:(Vertex *)vertices];
    }

    - (void)setScale:(float)x :(float)y :(float)z {
        [super setScale:x :y :z];
        [self setBoundingBox:(Vertex *)vertices];
    }
@end
