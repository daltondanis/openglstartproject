//
//  PModel.h
//  OpenGLObjectiveCBulletProject
//
//  Created by Dalton Danis on 2018-04-10.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"
#include "btBulletDynamicsCommon.h"

#define kBallTag    1
#define kBrickTag   2
#define kPaddleTag  3
#define kBorderTag  4

@interface PModel : Model
    @property (nonatomic, readonly) btRigidBody* body;
    @property (nonatomic, assign) int tag;

    - (instancetype)initWithName:(char *)name mass:(float)mass   //1
                          convex:(BOOL)convex                    //2
                             tag:(int)tag                        //3
                          shader:(BaseEffect *)shader
                        vertices:(Vertex *)vertices
                     vertexCount:(unsigned int)vertexCount;
    //                 textureName:(NSString *)textureName
    //               specularColor:(GLKVector4)specularColor
    //                diffuseColor:(GLKVector4)diffuseColor
    //                   shininess:(float)shininess;

@end
