//
//  GameScene.h
//  OpenGLObjectiveCBulletProject
//
//  Created by Dalton Danis on 2018-04-10.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"
#import "TextureManager.h"

@interface GameScene : Model
    @property (nonatomic, strong) UIView *view;
    @property (nonatomic) GLKMatrix4 projectionMatrix;
    @property (nonatomic) TextureManager* textureManager;
    - (instancetype)initWithShader:(BaseEffect *)shader;
    - (void)renderModels;
@end

