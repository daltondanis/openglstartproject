//
//  GameScene.m
//  OpenGLObjectiveCBulletProject
//
//  Created by Dalton Danis on 2018-04-10.
//  Copyright © 2018 opengl. All rights reserved.
//
#include "btBulletDynamicsCommon.h"
#include <OpenGLES/ES2/glext.h>
#include <GLKit/GLkit.h>

#import <Foundation/Foundation.h>
#import "GameScene.h"
#import "PhysicsDebugDrawer.h"
#import "LineRenderer.h"

//Objects
#import "ExampleCubeModel.h"
#import "ExampleBorderModel.h"
#import "ExampleCircleModel.h"

@implementation GameScene {
    CGSize _gameArea;
    float _sceneOffset;
    
    CGPoint _previousTouchLocation;
    float _ballVelocityX;
    float _ballVelocityY;
    
    btBroadphaseInterface*                  _broadphase;
    btDefaultCollisionConfiguration*        _collisionConfiguration;
    btCollisionDispatcher*                  _dispatcher;
    btSequentialImpulseConstraintSolver*    _solver;
    btDiscreteDynamicsWorld*                _world;
    btScalar                                _desiredVelocity;
    
    //Objects
    ExampleCubeModel* _cube;
}

//MARK: Debugging Rigid Body
//if this is turned true, compiling to devices will slow down
const bool debug = false;

- (instancetype)initWithShader:(BaseEffect*)shader {
    //MARK: Makes an empty model
    if ((self = [super initWithName:"GameScene" shader:shader vertices:nil vertexCount:0])) {
        // Initializing Bullet Pgysics
        [self initPhysics];
        
        // Debug
        if (debug) {
            PhysicsDebugDrawer *debugDrawer = new PhysicsDebugDrawer();
            _world->setDebugDrawer(debugDrawer);
        }
        
        // Create initial scene position (i.e. camera)
        _gameArea = CGSizeMake(27, 48);
        _sceneOffset = _gameArea.height/2 / tanf(GLKMathRadiansToDegrees(85.0/2));
        self.position = GLKVector3Make(-_gameArea.width/2, -_gameArea.height/2 + 10, -_sceneOffset);
        NSLog(@"Game X: %f Y: %f Z: %f", self.position.x, self.position.y, self.position.z);
        self.rotationX = GLKMathDegreesToRadians(-45);
    }
    return self;
}

- (void)renderModels {
    NSLog(@"Creating Cube");
    
    _cube = [[ExampleCubeModel alloc] initWithShader: self.shader];
    GLuint texture = [_textureManager getTexture:1];
    [_cube setTextureFile: texture];
    [_cube setColor:1.0 :0.0 :0.0];
    [_cube setPosition:_gameArea.width/2 :_gameArea.height * 0.0 :0];
    [_cube setScale:2.0];
    [self.children addObject:_cube];
    _world->addRigidBody(_cube.body);
    
    ExampleBorderModel* tmp = [[ExampleBorderModel alloc] initWithShader: self.shader];
    [tmp setColor:1.0 :0.0 :0.0];
    [tmp setPosition:_gameArea.width/2 :_gameArea.height * -0.5 :0];
    [tmp setScale:20 :1 :1];
    [self.children addObject:tmp];
    _world->addRigidBody(tmp.body);
    
    ExampleCircleModel* tmp2 = [[ExampleCircleModel alloc] initWithShader: self.shader];
    GLuint texture2 = [_textureManager getTexture:0];
    [tmp2 setTextureFile: texture2];
    [tmp2 setColor:1.0 :0.0 :0.0];
    [tmp2 setPosition:_gameArea.width/2 * 0.5 :_gameArea.height * -0.10 :0];
    [tmp2 setScale:10.0];
    [self.children addObject:tmp2];
    _world->addRigidBody(tmp2.body);
    
    
    //        // Create paddle near bottom of screen
    //        _paddle = [[RWPaddle alloc] initWithShader:shader];
    //        _paddle.position = GLKVector3Make(_gameArea.width/2, _gameArea.height * 0.05, 0);
    //        _paddle.diffuseColor = GLKVector4Make(1, 0, 0, 1);
    //        [self.children addObject:_paddle];
    //        _world->addRigidBody(_paddle.body);
    
    //        // Create ball right above paddle
    //        _ball = [[RWBall alloc] initWithShader:shader];
    //        _ball.position = GLKVector3Make(_gameArea.width/2, _gameArea.height * 0.1, 0);
    //        _ball.diffuseColor = GLKVector4Make(0.5, 0.9, 0, 1);
    //        [self.children addObject:_ball];
    //        _world->addRigidBody(_ball.body); //Adding ball to world
            _cube.body->setLinearVelocity(btVector3(0,-15,0));
            _desiredVelocity = _cube.body->getLinearVelocity().length();
    
    //        // Add border in center of screen
    //        _border = [[RWBorder alloc] initWithShader:shader];
    //        _border.position = GLKVector3Make(_gameArea.width/2, _gameArea.height/2, 0);
    //        [self.children addObject:_border];
    //        _world->addRigidBody(_border.body);
    
    //        // Generate colors for bricks
    //        GLKVector4 colors[BRICKS_PER_ROW];
    //        for (int i = 0; i < BRICKS_PER_ROW; i++) {
    //            colors[i] = [self color:(float)(BRICKS_PER_ROW-i) / (float)BRICKS_PER_ROW];
    //        }
    
    //        // Generate array of bricks
    //        _bricks = [NSMutableArray arrayWithCapacity:72];
    //        for (int j = 0; j < BRICKS_PER_COL; ++j)
    //        {
    //            for (int i = 0; i < BRICKS_PER_ROW; ++i)
    //            {
    //                RWBrick *brick = [[RWBrick alloc] initWithShader:shader];
    //                float margin = _gameArea.width * 0.1;
    //                float startY = _gameArea.height * 0.5;
    //                brick.position = GLKVector3Make(margin + (margin * i), startY + (margin * j), 0);
    //                brick.diffuseColor = colors[i];
    //                [self.children addObject:brick];
    //                [_bricks addObject:brick];
    //                _world->addRigidBody(brick.body);
    //            }
    //        }
}

-(void)initPhysics
{
    //1
    _broadphase = new btDbvtBroadphase();
    
    //2
    _collisionConfiguration = new btDefaultCollisionConfiguration();
    _dispatcher = new btCollisionDispatcher(_collisionConfiguration);
    
    //3
    _solver = new btSequentialImpulseConstraintSolver();
    
    //4
    _world = new btDiscreteDynamicsWorld(_dispatcher, _broadphase, _solver, _collisionConfiguration);
    
    //MARK: Gravity
    _world->setGravity(btVector3(0, 0, 0));
}

- (void)dealloc
{
    delete _world;
    delete _solver;
    delete _collisionConfiguration;
    delete _dispatcher;
    delete _broadphase;
}

// http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
- (GLKVector4)color:(float)x {
    float r = 0.0f;
    float g = 0.0f;
    float b = 1.0f;
    if (x >= 0.0f && x < 0.2f) {
        x = x / 0.2f;
        r = 0.0f;
        g = x;
        b = 1.0f;
    } else if (x >= 0.2f && x < 0.4f) {
        x = (x - 0.2f) / 0.2f;
        r = 0.0f;
        g = 1.0f;
        b = 1.0f - x;
    } else if (x >= 0.4f && x < 0.6f) {
        x = (x - 0.4f) / 0.2f;
        r = x;
        g = 1.0f;
        b = 0.0f;
    } else if (x >= 0.6f && x < 0.8f) {
        x = (x - 0.6f) / 0.2f;
        r = 1.0f;
        g = 1.0f - x;
        b = 0.0f;
    } else if (x >= 0.8f && x <= 1.0f) {
        x = (x - 0.8f) / 0.2f;
        r = 1.0f;
        g = 0.0f;
        b = x;
    }
    return GLKVector4Make(r, g, b, 1.0);
}

- (CGPoint)touchLocationToGameArea:(CGPoint)touchLocation {
    
    // Perform calculation to convert touch location to game area
    float ratio = self.view.frame.size.height / _gameArea.height;
    float actualX = touchLocation.x / ratio;
    float actualY = (self.view.frame.size.height - touchLocation.y) / ratio;
    CGPoint actual = CGPointMake(actualX, actualY);

    NSLog(@"Actual touch: %@", NSStringFromCGPoint(actual));
    return actual;
    return CGPointMake(0, 0);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Store previous touch location
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    _previousTouchLocation = [self touchLocationToGameArea:touchLocation];
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // Get current touch location
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    touchLocation = [self touchLocationToGameArea:touchLocation];

    // Calculate diff between previous touch location and current touch location
    CGPoint diff = CGPointMake(touchLocation.x - _previousTouchLocation.x, touchLocation.y - _previousTouchLocation.y);
    _previousTouchLocation = touchLocation;

    // Move cube position based on the diff
    float newX = _cube.position.x + diff.x;
//    newX = MIN(MAX(newX, _cube.width/2), _cube.width - _cube.width/2);
    NSLog(@"Cubes New X: %f", newX);
    _cube.position = GLKVector3Make(newX, _cube.position.y, _cube.position.z);
    
    // Move paddle's position based on the diff
//    float newX = _paddle.position.x + diff.x;
//    newX = MIN(MAX(newX, _paddle.width/2), _gameArea.width - _paddle.width/2);
//    _paddle.position = GLKVector3Make(newX, _paddle.position.y, _paddle.position.z);
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)renderWithParentModelViewMatrix:(GLKMatrix4)parentModelViewMatrix {
    if (debug) {
        GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply(parentModelViewMatrix, [self modelMatrix]);
        
        //[super renderWithParentModelViewMatrix:parentModelViewMatrix];
        LineRenderer* lineRenderer = [[LineRenderer alloc] initWithVertexShader:@"LineVertexShader.glsl" fragmentShader:@"LineFragmentShader.glsl"];
        lineRenderer.projectionMatrix = _projectionMatrix;
        lineRenderer.modelViewMatrix = modelViewMatrix;
        [lineRenderer prepareToDraw];
        
        _world->debugDrawWorld();
        
        //MARK: Debug for RigidBox
        //    [_world debugDraw];
    } else {
        [super renderWithParentModelViewMatrix:parentModelViewMatrix];
    }
}

- (void)updateWithDelta:(NSTimeInterval)dt {
    //1
    [super updateWithDelta:dt];
    
    //2
    _world->stepSimulation(dt);
    
    //3
    //NSLog(@"Ball height: %f", _ball.body->getWorldTransform().getOrigin().getY());
    
//    if (_ball.position.y < 0)
//    {
//        [RWDirector sharedInstance].scene = [[RWGameOverScene alloc] initWithShader:self.shader win:NO];
//        return;
//    }
    
    //1
//    int numManifolds = _world->getDispatcher()->getNumManifolds();
//    for (int i=0;i<numManifolds;i++)
//    {
//        //2
//        btPersistentManifold* contactManifold =  _world->getDispatcher()->getManifoldByIndexInternal(i);
//
//        //3
//        int numContacts = contactManifold->getNumContacts();
//        if (numContacts > 0)
//        {
//            //4
//            [[RWDirector sharedInstance] playPopEffect];
//
//            //5
//            const btCollisionObject* obA = contactManifold->getBody0();
//            const btCollisionObject* obB = contactManifold->getBody1();
//
//            //6
//            PNode* pnA = (__bridge PNode*)obA->getUserPointer();
//            PNode* pnB = (__bridge PNode*)obB->getUserPointer();
//
//            //7
//            if (pnA.tag == kBrickTag) {
//                [self destroyBrickAndCheckVictory:pnA];
//            }
//
//            //8
//            if (pnB.tag == kBrickTag){
//                [self destroyBrickAndCheckVictory:pnB];
//            }
//        }
//    }
    
    btVector3 currentVelocityDirection = _cube.body->getLinearVelocity();
    btScalar currentVelocty = currentVelocityDirection.length();
    if (currentVelocty < _desiredVelocity)
    {
        currentVelocityDirection *= _desiredVelocity/currentVelocty;
        _cube.body->setLinearVelocity(currentVelocityDirection);
    }
    
}

//- (void)destroyBrickAndCheckVictory:(PNode*)brick
//{
//    //1
//    [self.children removeObject:brick];
//    [_bricks removeObject:brick];
//
//    //2
//    _world->removeRigidBody(brick.body);
//
//    //3
//    if (_bricks.count == 0) {
//        [RWDirector sharedInstance].scene = [[RWGameOverScene alloc] initWithShader:self.shader win:YES];
//    }
//}

@end
