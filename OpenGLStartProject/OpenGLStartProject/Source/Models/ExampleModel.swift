//
//  ExampleModel.swift
//  OpenGLStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

import Foundation
import GLKit

//MARK: This is a plain or pannel
class ExampleModel : Model {
    var id : Int = 0
    
    var vertexList : [Vertex] = [
        //       x     y   z   r    g    b    a    u    v  nx ny nz
        Vertex( 1.0, -1.0, 0, 1.0, 0.0, 0.0, 1.0, 1, 0, 0, 0, 1),
        Vertex( 1.0,  1.0, 0, 0.0, 1.0, 0.0, 1.0, 1, 1, 0, 0, 1),
        Vertex(-1.0,  1.0, 0, 0.0, 0.0, 1.0, 1.0, 0, 1, 0, 0, 1),
        Vertex(-1.0, -1.0, 0, 1.0, 1.0, 0.0, 1.0, 0, 0, 0, 0, 1)
    ]
    
    let indexList : [GLuint] = [
        0, 1, 2,
        2, 3, 0
    ]
    
    //MARK: Initializers
    init(shader: BaseEffect) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
    }
    
    init(shader: BaseEffect, texture: GLuint) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
        if (texture == 0) {
            self.setTexture(file: texture)
        }
    }
    
    //MARK: Called every time delta (Update)
    override func updateWithDelta(_ dt: TimeInterval) {
        //        let secsPerMove = 2.0
        //        self.position = GLKVector3Make(
        //            Float(sin(CACurrentMediaTime() * 2 * Double.pi / secsPerMove)),
        //            self.position.y,
        //            self.position.z)
    }
    
    override func renderWithParentMoelViewMatrix(_ parentModelViewMatrix: GLKMatrix4) {
        //glUniform4f(self.shader.colorUniform, GLfloat(r), GLfloat(g), GLfloat(b), 1)
        super.renderWithParentMoelViewMatrix(parentModelViewMatrix)
    }
    
    override func getModelInfo() {
        super.getModelInfo()
        print("ExampleModel Info")
    }
}
