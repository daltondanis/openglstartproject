//
//  ViewController.swift
//  OpenGLStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

import UIKit
import GLKit

class GLKUpdater : NSObject, GLKViewControllerDelegate {
    weak var glkViewController : ViewController!
    
    init(glkViewController : ViewController) {
        self.glkViewController = glkViewController
    }
    
    //MARK: UPDATE OPENGL FUNCTION
    func glkViewControllerUpdate(_ controller: GLKViewController) {
        //MARK: This is what makes the model spin!
        if (glkViewController.model != nil) {
//            glkViewController.model.updateWithDelta(self.glkViewController.timeSinceLastUpdate)
        }
    }
}

class ViewController: GLKViewController {
    var glkView: GLKView!
    var glkUpdater: GLKUpdater!
    var shader : BaseEffect!
    
    //MARK: Managers
    var textureManager : TextureManager!
    
    //MARK: Camera
    let viewMatrix : GLKMatrix4 = GLKMatrix4MakeTranslation(0, 0, -5)
    
    //MARK: Models
    var temp : Model!
    var model : ExampleModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupGLcontext()
        self.setupGLupdater()
        self.setupScene() //Setups the Scene
        self.setupTextures() //Setup Textures
        self.setupModels() //Setup the Models to be rendered
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        //Transfomr4: Viewport: Normalized -> Window
        //glViewport(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        
        //Sky Colour
        glClearColor(Settings.bg_red, Settings.bg_green, Settings.bg_blue, 1.0);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT))
        
        glEnable(GLenum(GL_DEPTH_TEST))
        glEnable(GLenum(GL_CULL_FACE))
        glEnable(GLenum(GL_BLEND))
        glBlendFunc(GLenum(GL_SRC_ALPHA), GLenum(GL_ONE_MINUS_SRC_ALPHA))
        
        //MARK: Render Updated Models Here!
        if (self.model != nil) {
            self.model.renderWithParentMoelViewMatrix(self.viewMatrix)
        }
    }
}

extension ViewController {
    
    func setupGLcontext() {
        glkView = self.view as! GLKView
        glkView.context = EAGLContext(api: .openGLES2)!
        glkView.drawableDepthFormat = .format16         // for depth testing
        EAGLContext.setCurrent(glkView.context)
    }
    
    func setupGLupdater() {
        self.glkUpdater = GLKUpdater(glkViewController: self)
        self.delegate = self.glkUpdater
    }
    
    func setupScene() {
        //MARK: Creates the glsl shader files
        self.shader = BaseEffect(vertexShader: "SimpleVertexShader.glsl", fragmentShader: "SimpleFragmentShader.glsl")
        //MARK: Camera Persepective
        self.shader.projectionMatrix = GLKMatrix4MakePerspective(
            GLKMathDegreesToRadians(90.0),
            GLfloat(self.view.bounds.size.width / self.view.bounds.size.height),
            1,
            150
        )
    }
    
    func setupTextures() {
        self.textureManager = TextureManager()
    }
    
    func setupModels() {
        self.model = ExampleModel(shader: self.shader)
        self.model.setTexture(file: self.textureManager.textures[0])
        self.model.setColor(red: 0.0, green: 0.0, blue: 1.0);
        self.model.setPosition(x: 0.0, y: 0.0, z: 0.0)
        self.model.setScale(scale: 0.5)
    }
}
