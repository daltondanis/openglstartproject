//
//  Settings.swift
//  OpenGLStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

import Foundation
import OpenGLES
import GLKit

struct Settings {
    //MARK: Examples
    static var exampeInt : Int = 0
    static var exampeString : String = "Settings Example"
    
    //MARK: Background colors
    static var bg_red : Float = 1.0
    static var bg_green : Float = 0.0
    static var bg_blue : Float = 0.0
}
