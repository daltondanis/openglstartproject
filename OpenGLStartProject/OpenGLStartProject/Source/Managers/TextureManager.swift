//
//  TextureManager.swift
//  OpenGLStartProject
//
//  Created by Dalton Danis on 2018-04-08.
//  Copyright © 2018 opengl. All rights reserved.
//

import GLKit
import Foundation

class TextureManager {
    var textures : Array<GLuint> = Array()
    
    //MARK: to load a texture put the following in the array:
    //Loads in the texture files by <filename>.<filetype>
    private var filenames : Array<String> = [
        "brick.png",
        "dice.png"
    ]
    
    init() {
        self.loadTextures()
    }
    
    private func textureLoader(file: String) -> GLuint {
        let path = Bundle.main.path(forResource: file, ofType: nil)!
        let option = [GLKTextureLoaderOriginBottomLeft: true]
        do {
            let info = try GLKTextureLoader.texture(withContentsOfFile: path, options: option as [String : NSNumber]?)
            return info.name
        } catch {
            NSLog("Failed to load model texture \(file)")
        }
        return (0) as GLuint
    }
    
    private func loadTextures() {
        var count = 0
        for file in filenames {
            self.textures.insert(self.textureLoader(file: file), at: count)
            count += 1;
        }
    }
}

